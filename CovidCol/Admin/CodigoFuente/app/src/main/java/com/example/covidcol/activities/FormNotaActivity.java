package com.example.covidcol.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.covidcol.R;
import com.example.covidcol.fragmen.CategoriaFragment;
import com.example.covidcol.fragmen.TipoFragment;
import com.example.covidcol.fragmen.UsuarioFragment;
import com.example.covidcol.persistencia.model.NotaModel;
import com.example.covidcol.persistencia.respositorio.NotaRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FormNotaActivity extends AppCompatActivity {

    @BindView(R.id.txtTitulo)
    EditText txtTitulo;

    @BindView(R.id.txtContenido)
    EditText txtContenido;

    @BindView(R.id.editTextDate)
    EditText editTextDate;

    UsuarioFragment fgtUsuario;


    @BindView(R.id.btnCrearNota)
    Button btnCrearNota;
    private int mYearIni, mMonthIni, mDayIni, sYearIni, sMonthIni, sDayIni;
    static final int DATE_ID = 0;
    Calendar C = Calendar.getInstance();
    NotaModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_nota);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        model = new NotaModel();
        sMonthIni = C.get(Calendar.MONTH);
        sDayIni = C.get(Calendar.DAY_OF_MONTH);
        sYearIni = C.get(Calendar.YEAR);
        editTextDate = (EditText) findViewById(R.id.editTextDate);

        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(DATE_ID);
            }
        });
    }

    public void onClickCrearNota(View view) throws ParseException {

        model.setTitulo(txtTitulo.getText().toString());
        model.setContenido(txtContenido.getText().toString());
        model.setFecha(Calendar.getInstance().getTime());
        fgtUsuario = (UsuarioFragment) getSupportFragmentManager().findFragmentById(R.id.fgtUsuario);

        model.setNombreUsuario( fgtUsuario.getUsuarioSeleccionado() );
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = dateFormat.parse(editTextDate.getText().toString());

        model.setFechaRegistro(date);

        NotaRepository.getInstance().crear(model, o -> {
            NotaModel notaModel = (NotaModel) o;
            FormNotaActivity.this.successDialog("Nota creada exitosamente").show();
            System.out.println(o.toString());
        },e -> {
            Toast.makeText(this, "error: " + e.toString(), Toast.LENGTH_LONG).show();
        });
    }

    public static class Data {
        public static final String NOTA_MODEL = "NOTA_MODEL";
        public static final String BUNDLE = "BUNDLE";
    }

    private Dialog successDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                });

        return builder.create();
    }
    private void colocar_fecha() {
        editTextDate.setText((mMonthIni + 1) + "-" + mDayIni + "-" + mYearIni+" ");
    }



    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    mYearIni = year;
                    mMonthIni = monthOfYear;
                    mDayIni = dayOfMonth;
                    colocar_fecha();

                }

            };


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                return new DatePickerDialog(this, mDateSetListener, sYearIni, sMonthIni, sDayIni);


        }


        return null;
    }
}
