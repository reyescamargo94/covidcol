package com.example.covidcol.fragmen.notasfragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.covidcol.R;
import com.example.covidcol.persistencia.model.NotaModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class MyNotaRecyclerViewAdapter extends RecyclerView.Adapter<MyNotaRecyclerViewAdapter.ViewHolder> implements Filterable {

    private  List<NotaModel> mValues;
    private  List<NotaModel> listaCopia;

    public MyNotaRecyclerViewAdapter(List<NotaModel> items) {
        listaCopia = new ArrayList<>(items);
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_nota, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getTitulo());
        holder.mContentView.setText(mValues.get(position).getContenido());

        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd/MM/yyyy");
        String dateaux = simpleDate.format(mValues.get(position).getFechaRegistro());

        holder.mDateView.setText(dateaux);
        holder.mNombreUsuarioView.setText(mValues.get(position).getNombreUsuario());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(holder.mView.getContext(), "hola", Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void add(List<NotaModel> notaModelList) {
        mValues.clear();
        mValues.addAll(notaModelList);
        listaCopia = new ArrayList<>(notaModelList);
        this.notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filtro;
    }

    private Filter filtro = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<NotaModel> listafiltrado = new ArrayList<>( );
            if (charSequence == null || charSequence.length() == 0) {
                listafiltrado.addAll(listaCopia);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (NotaModel item : mValues) {
                    if (item.getNombreUsuario().toLowerCase().contains(filterPattern)) {
                        listafiltrado.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = listafiltrado;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
            mValues.clear();
            mValues.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mDateView;
        public final TextView mNombreUsuarioView;
        public NotaModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDateView = (TextView) view.findViewById(R.id.item_date);
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            mNombreUsuarioView= (TextView) view.findViewById(R.id.item_nombreusuario);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
