package com.example.covidcol.persistencia.respositorio;

import com.example.covidcol.persistencia.model.PuntoInteresModel;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.Query;

public class PuntoInteresRepository extends Repository<PuntoInteresModel> {
    @Override
    protected Query queryFilter(PuntoInteresModel filtro, CollectionReference collection) {
        Query query = collection;
        query = (filtro.getId() != null)? query.whereEqualTo(PuntoInteresModel.Key.ID, filtro.getId()): query;
        query = (filtro.getNombrePuntoInteres() != null)? query.whereEqualTo(PuntoInteresModel.Key.NOMBRE_PUNTO_INTERES, filtro.getNombrePuntoInteres()): query;
        query = (filtro.getDireccion() != null)? query.whereEqualTo(PuntoInteresModel.Key.DIRECCION, filtro.getDireccion()): query;
        query = (filtro.getCiudad() != null)? query.whereEqualTo(PuntoInteresModel.Key.CIUDAD, filtro.getCiudad()): query;
        query = (filtro.getDepartamento() != null)? query.whereEqualTo(PuntoInteresModel.Key.DEPARTAMENTO, filtro.getDepartamento()): query;
        query = (filtro.getPais() != null)? query.whereEqualTo(PuntoInteresModel.Key.PAIS, filtro.getPais()): query;
        query = (filtro.getCategoria() != null)? query.whereEqualTo(PuntoInteresModel.Key.categoria, filtro.getCategoria()): query;
        query = (filtro.getTipo() != null)? query.whereEqualTo(PuntoInteresModel.Key.tipo, filtro.getTipo()): query;
        query = (filtro.getNombre() != null)? query.whereEqualTo(PuntoInteresModel.Key.nombre, filtro.getNombre()): query;
        query = (filtro.getUbicacion() != null)? query.whereEqualTo(PuntoInteresModel.Key.UBICACION, filtro.getUbicacion()): query;
        query = (filtro.getHorario() != null)? query.whereEqualTo(PuntoInteresModel.Key.HORARIO, filtro.getHorario()): query;
        query = (filtro.getNombrePrograma() != null)? query.whereEqualTo(PuntoInteresModel.Key.NOMBRE_PROGRAMA, filtro.getNombrePrograma()): query;
        query = (filtro.getPersonaContacto() != null)? query.whereEqualTo(PuntoInteresModel.Key.PERSONA_CONTACTO, filtro.getPersonaContacto()): query;
        query = (filtro.getEmail() != null)? query.whereEqualTo(PuntoInteresModel.Key.EMAIL, filtro.getEmail()): query;
        query = (filtro.getLatitud() != null)? query.whereEqualTo(PuntoInteresModel.Key.LATITUD, filtro.getLatitud()): query;
        query = (filtro.getLongitud() != null)? query.whereEqualTo(PuntoInteresModel.Key.LONGITUD, filtro.getLongitud()): query;
        return query;
    }

    @Override
    protected Class getClassModel() {
        return PuntoInteresModel.class;
    }

    public static Repository getInstance(){
        if(intance == null){
            intance = new PuntoInteresRepository();
        }
        return intance;
    }
}
