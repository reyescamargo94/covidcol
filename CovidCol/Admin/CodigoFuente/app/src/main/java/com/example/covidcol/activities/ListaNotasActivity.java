package com.example.covidcol.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.covidcol.fragmen.notasfragment.ListaNotasFragment;
import com.example.covidcol.fragmen.notasfragment.MyNotaRecyclerViewAdapter;
import com.example.covidcol.persistencia.model.NotaModel;
import com.example.covidcol.persistencia.respositorio.NotaRepository;
import com.example.covidcol.persistencia.respositorio.Repository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.example.covidcol.R;

import java.util.List;

public class ListaNotasActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int CREAR_NOTA = 0x0001;
    private FloatingActionButton fab;
    private ListaNotasFragment listaNotasFragment;
    private MyNotaRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);
        setTitle("Seguimiento");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
        listaNotasFragment = (ListaNotasFragment) getSupportFragmentManager().findFragmentById(R.id.frgListaNotas);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Repository repository = NotaRepository.getInstance();
        repository.buscar( new NotaModel(),list -> {
            List<NotaModel> notaModelList = (List<NotaModel>) list;
            ListaNotasActivity.this.loadNotas(notaModelList);
        }, e -> {
            Toast.makeText(this,"Error inesperado", Toast.LENGTH_LONG).show();
        });
    }

    private void loadNotas(List<NotaModel> notaModelList) {
        adapter = listaNotasFragment.loadNotas(notaModelList);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,FormNotaActivity.class);
        startActivityForResult(intent, CREAR_NOTA);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_nota,menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView buscador = (SearchView) item.getActionView();
        // teclado salga la palomita
        buscador.setImeOptions(EditorInfo.IME_ACTION_DONE);

        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
