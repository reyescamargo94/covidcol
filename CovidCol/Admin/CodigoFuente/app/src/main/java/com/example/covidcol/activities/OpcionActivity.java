package com.example.covidcol.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.covidcol.R;
import com.example.covidcol.persistencia.model.TipoCovidModel;
import com.example.covidcol.persistencia.respositorio.Repository;
import com.example.covidcol.persistencia.respositorio.TipoCovidRepository;

import java.util.List;
import java.util.function.Consumer;

public class OpcionActivity extends AppCompatActivity {

    private static final String TAG = OpcionActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcion);
    }

    public void onClickUbicaion(View view){
        Intent intent = new Intent(this.getApplicationContext(), UbicacionActivity.class);
        startActivity(intent);
    }

    public void crear(View view){
        TipoCovidModel tipo = new TipoCovidModel(null, "LLANTA", "llantas");
        Repository repository = TipoCovidRepository.getInstance();
        repository.crear(tipo, o -> {
            if(o != null){
                Toast.makeText(OpcionActivity.this,"CREADO",Toast.LENGTH_LONG).show();
            }
        }, new Consumer<Exception>() {
            @Override
            public void accept(Exception e) {
                Log.e(OpcionActivity.TAG, "accept: "+e.getMessage(), e);
            }
        });

    }

    public void buscar(View view){
        TipoCovidModel filtro = new TipoCovidModel();
        filtro.setCodigo("LLANTA");
        Repository repository = TipoCovidRepository.getInstance();
        repository.buscar(filtro, new Consumer<List>() {
            @Override
            public void accept(List list) {
                if (list.size() > 0) {
                    Toast.makeText(OpcionActivity.this, "EXITO", Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "Buscar: ------------- NO DATOS");
                }
            }
        }, new Consumer<Exception>() {
            @Override
            public void accept(Exception e) {
                Log.e(OpcionActivity.TAG, "Error buscar: "+e.getMessage(), e);
            }
        });

    }

    public void buscarRef(View view){
        TipoCovidModel filtro = new TipoCovidModel();
        filtro.setId( "kHff6XrDnma7Qqt7bmiW");
        TipoCovidRepository.getInstance().buscarReferencia(filtro, o -> {
            Log.d(TAG, "buscarRef: "+o.toString());
        }, e -> {
            Log.e(OpcionActivity.TAG, "Error buscar: ", (Throwable) e);
        });
    }

    public void onClickPuntoCovid(View view){
        Intent intent = new Intent(this, PuntoCovidActivity.class);
        startActivity(intent);
    }

    public void onClickNotas(View view){
        Intent intent = new Intent(this, ListaNotasActivity.class);
        startActivity(intent);
    }

    public void onClickAprendeCovid(View view){
        Intent intent = new Intent(this, AprenderActivity.class);
        startActivity(intent);
    }

    public void onClickEstadisticas(View view){
        Intent intent = new Intent(this, GraficoActivity.class);
        startActivity(intent);
    }

    public void onClickChat(View view){
        Intent intent = new Intent(this, ListaChatActivity.class);
        startActivity(intent);
    }

    public void onClickAcercade(View view){
        Intent intent = new Intent(this, AcercadeActivity.class);
        startActivity(intent);
    }
    public void onClickVacuna(View view){
        Intent intent = new Intent(this, Vacuna.class);
        startActivity(intent);
    }
}
