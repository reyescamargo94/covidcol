package com.example.covidcol.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.covidcol.R;

import java.util.ArrayList;
import java.util.Arrays;

public class ListaChatActivity extends AppCompatActivity {

    ListView usersList;

    TextView noUsersText;

    ArrayList al = new ArrayList<>();

    int totalUsers = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_chat);
        usersList = (ListView)findViewById(R.id.usersList);
        noUsersText = (TextView)findViewById(R.id.noUsersText);
        al = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.usuario)));
        usersList.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, al));
        usersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(ListaChatActivity.this, ChatActivity.class));
            }
        });
    }
}