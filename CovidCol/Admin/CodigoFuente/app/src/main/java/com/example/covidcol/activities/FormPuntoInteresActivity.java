package com.example.covidcol.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.covidcol.R;
import com.example.covidcol.fragmen.CategoriaFragment;
import com.example.covidcol.fragmen.TipoFragment;
import com.example.covidcol.persistencia.model.PuntoInteresModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FormPuntoInteresActivity extends AppCompatActivity {

    private PuntoInteresModel model;

    @BindView(R.id.txtNombrePuntointeres)
    EditText txtNombrePuntointeres;

    @BindView(R.id.txtDireccion)
    EditText txtDireccion;

    @BindView(R.id.txtCiudad)
    EditText txtCiudad;

    @BindView(R.id.txtDepartamento)
    EditText txtDepartamento;

    @BindView(R.id.txtPais)
    EditText txtPais;

//    @BindView(R.id.txtCategoria)
//    EditText txtCategoria;

    // @BindView(R.id.fgtCategoria)
    CategoriaFragment fgtCategoria;

//    @BindView(R.id.txtTipo)
//    EditText txtTipo;

    // @BindView(R.id.fgtTipo)
    TipoFragment fgtTipo;

    @BindView(R.id.txtNombre)
    EditText txtNombre;

    @BindView(R.id.txtUbicacion)
    EditText txtUbicacion;

    @BindView(R.id.txtHorario)
    EditText txtHorario;

    @BindView(R.id.txtProgramaPostconsumo)
    EditText txtProgramaPostconsumo;

    @BindView(R.id.txtPersonaContacto)
    EditText txtPersonaContacto;

    @BindView(R.id.txtEmail)
    EditText txtEmail;


    @BindView(R.id.btnAceptar)
    Button btnAceptar;

    @BindView(R.id.btnCancelar)
    Button btnCancelar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_punto_covid);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        Bundle bundleExtra = intent.getBundleExtra(Data.BUNDLE);
        model = (PuntoInteresModel) bundleExtra.getSerializable(Data.PUNTO_INTERES_MODEL);
        txtDireccion.setText(model.getDireccion());
        txtCiudad.setText(model.getCiudad());
        txtDepartamento.setText(model.getDepartamento());
        txtPais.setText(model.getPais());
        txtUbicacion.setText(model.getUbicacion());

        fgtCategoria = (CategoriaFragment) getSupportFragmentManager().findFragmentById(R.id.fgtCategoria);
        fgtTipo = (TipoFragment) getSupportFragmentManager().findFragmentById(R.id.fgtTipo);

        setTitle(R.string.title_form_punto_interes);
        if(bundleExtra.containsKey(Data.ACCION_NUEVO)){
            btnAceptar.setText("Crear");
        } else if(bundleExtra.containsKey(Data.ACCION_ACTUALIZAR)){
            btnAceptar.setText("Actualizar");
        }

    }

    public static class Data {
        public static final String PUNTO_INTERES_MODEL = "PUNTO_INTERES_MODEL";
        public static final String BUNDLE = "BUNDLE";
        public static final String ACCION_NUEVO = "ACCION_NUEVO";
        public static final String ACCION_ACTUALIZAR = "ACCION_ACTUALIZAR";
    }

    public void onClickAceptar(View view){
        model.setNombrePuntoInteres(txtNombrePuntointeres.getText().toString());
        model.setDireccion(txtDireccion.getText().toString());
        model.setCiudad(txtCiudad.getText().toString());
        model.setDepartamento(txtDepartamento.getText().toString());
        model.setPais(txtPais.getText().toString());
        model.setCategoria(fgtCategoria.getCategoriaSeleccionada());
        model.setTipo(fgtTipo.getTipoSeleccionado());
        model.setNombre(txtNombre.getText().toString());
        model.setUbicacion(txtUbicacion.getText().toString());
        model.setHorario(txtHorario.getText().toString());
        model.setNombrePrograma(txtProgramaPostconsumo.getText().toString());
        model.setPersonaContacto(txtPersonaContacto.getText().toString());
        model.setEmail(txtEmail.getText().toString());

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Data.PUNTO_INTERES_MODEL, this.model);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public void onClickCancelar(View view){
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Data.PUNTO_INTERES_MODEL, this.model);
        intent.putExtra(Data.BUNDLE, bundle) ;
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}
