package com.example.covidcol.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.covidcol.R;
import com.example.covidcol.persistencia.model.VacunaModel;
import com.example.covidcol.persistencia.respositorio.VacunaRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Vacuna extends AppCompatActivity {

    EditText fecha;
    EditText fechaVacuna;
    EditText biologico;
    EditText dosis;
    EditText lote;
    EditText fabricante;
    EditText especialista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_vacuna );
        biologico = (EditText) findViewById(R.id.txtBiologico);
        dosis = (EditText) findViewById(R.id.txtDosis);
        lote= (EditText) findViewById(R.id.txtLote);
        fabricante = (EditText) findViewById(R.id.txtFabricante);
        especialista = (EditText) findViewById(R.id.txtEspecialista);
        fecha = (EditText) findViewById(R.id.txtFecha);
        fecha.setOnClickListener(v -> {
            showDatePickerDialog(fecha);
        });

        fechaVacuna = (EditText) findViewById(R.id.txtFechaVacuna);
        fechaVacuna.setOnClickListener(v -> {
            showDatePickerDialog(fechaVacuna);
        });
    }

    public void onClickVacuna(View view) throws ParseException {
        VacunaModel model = new VacunaModel();
        model.setBiologico(biologico.getText().toString());
        model.setLote(lote.getText().toString());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date fechadate = dateFormat.parse(fecha.getText().toString());
        model.setFecha(fechadate);
        Date fechadateproxima = dateFormat.parse(fechaVacuna.getText().toString());
        model.setFechaProxima(fechadateproxima);
        model.setEspecialista(especialista.getText().toString());

        VacunaRepository.getInstance().crear(model, o -> {
            this.successDialog("Vacuna Registrada exitosamente").show();
        }, e -> {
            Toast.makeText(this, "error: " + e.toString(), Toast.LENGTH_LONG).show();
        });


    }

    private Dialog successDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                });

        return builder.create();
    }

    private void showDatePickerDialog(EditText fecha) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                final String selectedDate = day + "-" + (month+1) + "-" + year;
                fecha.setText(selectedDate);
            }
        });

        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment {

        private DatePickerDialog.OnDateSetListener listener;

        public static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener listener) {
            DatePickerFragment fragment = new DatePickerFragment();
            fragment.setListener(listener);
            return fragment;
        }

        public void setListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
        }

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), listener, year, month, day);
        }

    }
}