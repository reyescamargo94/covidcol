package com.example.covidcol.persistencia.respositorio;

import com.example.covidcol.persistencia.model.TipoCovidModel;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.Query;

public class TipoCovidRepository extends Repository<TipoCovidModel> {

    @Override
    protected Query queryFilter(TipoCovidModel filtro, CollectionReference collection) {
        Query query = collection;
        query = (filtro.getId() != null)? query.whereEqualTo(TipoCovidModel.Key.ID, filtro.getId()): query;
        query = (filtro.getCodigo() != null)? query.whereEqualTo(TipoCovidModel.Key.CODIGO, filtro.getCodigo()): query;
        query = (filtro.getEtiqueta() != null)? query.whereEqualTo(TipoCovidModel.Key.ETIQUETA, filtro.getEtiqueta()): query;
        return query;
    }

    @Override
    protected Class getClassModel() {
        return TipoCovidModel.class;
    }



    public static Repository getInstance(){
        if(intance == null){
            intance = new TipoCovidRepository();
        }
        return intance;
    }
}
