package com.example.covidcol.persistencia.respositorio;


import com.example.covidcol.persistencia.model.VacunaModel;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.Query;

public class VacunaRepository extends Repository<VacunaModel> {


    @Override
    protected Query queryFilter(VacunaModel filtro, CollectionReference collection) {
        Query query = collection;
        query = (filtro.getId() != null)? query.whereEqualTo(VacunaModel.Key.ID, filtro.getId()): query;
        query = (filtro.getBiologico() != null)? query.whereEqualTo(VacunaModel.Key.BIOLOGICO, filtro.getBiologico()): query;
        query = (filtro.getFecha() != null)? query.whereEqualTo(VacunaModel.Key.FECHA, filtro.getFecha()): query;
        query = (filtro.getDosis() != null)? query.whereEqualTo(VacunaModel.Key.DOSIS, filtro.getDosis()): query;
        return query;
    }

    @Override
    protected Class getClassModel() {
        return VacunaModel.class;
    }

    public static Repository getInstance(){
        if(intance == null){
            intance = new VacunaRepository();
        }
        return intance;
    }
}
