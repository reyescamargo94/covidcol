package com.example.covidcol.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.covidcol.R;
import com.example.covidcol.fragmen.listafragment.ListasDePuntosPorCategorias;
import com.example.covidcol.fragmen.listafragment.PuntosCovidFragment;
import com.example.covidcol.util.Utils;

public class PuntoCovidActivity extends AppCompatActivity implements PuntosCovidFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punto_covid);

    }

    @Override
    public void onListFragmentInteraction(ListasDePuntosPorCategorias.FiltroPuntosCategoria item) {
        System.out.println(item.filtro.toString());
        Intent intent = new Intent(getBaseContext(), MapaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(MapaActivity.Data.PUNTO_INTERES_MODEL, item.filtro);
        bundle.putBoolean(MapaActivity.Data.ACCION_DIBUJAR_PUNTOS, Boolean.TRUE);
        intent.putExtra(MapaActivity.Data.BUNDLE, bundle);
        startActivity(intent);
    }
}
