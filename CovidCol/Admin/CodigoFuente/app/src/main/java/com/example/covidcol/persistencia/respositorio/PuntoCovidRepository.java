package com.example.covidcol.persistencia.respositorio;

import com.example.covidcol.persistencia.model.PuntoCovidModel;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.Query;

public class PuntoCovidRepository extends Repository<PuntoCovidModel> {
    @Override
    protected Query queryFilter(PuntoCovidModel filtro, CollectionReference collection) {
        Query query = collection;
        query = (filtro.getId() != null)? query.whereEqualTo(PuntoCovidModel.Key.ID, filtro.getId()): query;
        query = (filtro.getIdTipoCovid() != null)? query.whereEqualTo(PuntoCovidModel.Key.ID_TIPO_COVID, filtro.getIdTipoCovid()): query;
        query = (filtro.getIdUbicacion() != null)? query.whereEqualTo(PuntoCovidModel.Key.ID_UBICACION, filtro.getIdUbicacion()): query;
        return query;
    }

    @Override
    protected Class getClassModel() {
        return PuntoCovidModel.class;
    }

    public static Repository getInstance(){
        if(intance == null){
            intance = new PuntoCovidRepository();
        }
        return intance;
    }
}
