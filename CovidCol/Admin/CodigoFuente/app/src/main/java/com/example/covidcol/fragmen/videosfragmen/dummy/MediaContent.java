package com.example.covidcol.fragmen.videosfragmen.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MediaContent {

    public static final List<MediaItem> ITEMS = new ArrayList<MediaItem>();
    public static final Map<String, MediaItem> ITEM_MAP = new HashMap<String, MediaItem>();

    private static final int COUNT = 25;

    static {
        addItem(createDummyItem("¿Qué es el Covid-19? -- Fuente: Youtube", "https://youtu.be/gusQcaDlLw0"));
        addItem(createDummyItem("Covid-19 mapa de casos en vivo -- Fuente: Youtube", "https://www.youtube.com/watch?v=YwhL98NiCcc"));
        addItem(createDummyItem("Detener la propagación -- Fuente: Youtube", "https://youtu.be/TNOZZkG_EMY"));
        addItem(createDummyItem("Consejos para manejar el estrés con la pandemia -- Fuente: Youtube", "https://youtu.be/8cIhYlwNp8A"));
        addItem(createDummyItem("Cuidando a alguien enfermo con el Coronavirus -- Fuente: Youtube", "https://www.youtube.com/watch?v=OCtVx7e21XI"));
        addItem(createDummyItem("¿Una persona se puede volver a contagiarse? -- Fuente: Youtube", "https://www.youtube.com/watch?v=YwhL98NiCcc"));
        addItem(createDummyItem("Nutricción para subir el sistema inmune -- Fuente: Hacia un Nuevo Estilo de Vida Youtube", "https://www.youtube.com/watch?v=Brtonp0r5-I"));
        addItem(createDummyItem("Medicación para el coronavirus y psicología para el encierro en casa Fuente: Hacia un Nuevo Estilo de Vida Youtube", "https://www.youtube.com/watch?v=3MlrS-65rbo"));

    }

    private static void addItem(MediaItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static MediaItem createDummyItem(String titulo, String url) {
        return new MediaItem(String.valueOf(ITEMS.size() + 1),titulo, url);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    public static class MediaItem {
        public final String id;
        public final String content;
        public final String details;

        public MediaItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
