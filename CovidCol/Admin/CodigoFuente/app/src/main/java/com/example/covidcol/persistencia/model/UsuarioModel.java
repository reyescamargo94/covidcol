package com.example.covidcol.persistencia.model;

import java.util.HashMap;
import java.util.Map;

public class UsuarioModel extends Model {

    public static String NODE_NAME= "usuario";

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    private String usuario;
    private String nombre;

    public static class Key {
        public static final String ID = Model.Key.ID;
        public static final String USUARIO = "usuario";
        public static final String NOMBRE = "nombre";
    }

    @Override
    public String NODE_NAME() {
        return NODE_NAME;
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(Key.ID, getId());
        map.put(Key.USUARIO, getUsuario());
        map.put(Key.NOMBRE, getNombre());
        return map;
    }

}
