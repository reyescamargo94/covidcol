package com.example.covidcol.persistencia.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class VacunaModel extends Model {

    public static String NODE_NAME= "vacuna";



    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getBiologico() {
        return biologico;
    }

    public void setBiologico(String biologico) {
        this.biologico = biologico;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public Date getFechaProxima() {
        return fechaProxima;
    }

    public void setFechaProxima(Date fechaProxima) {
        this.fechaProxima = fechaProxima;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getEspecialista() {
        return especialista;
    }

    public void setEspecialista(String especialista) {
        this.especialista = especialista;
    }

    private String biologico;
    private String dosis;
    private Date fecha;
    private Date fechaProxima;
    private String fabricante;
    private String lote;
    private String especialista;

    public static class Key {
        public static final String ID = Model.Key.ID;
        public static final String BIOLOGICO = "biologico";
        public static final String DOSIS = "dosis";
        public static final String FECHA = "fecha";
        public static final String FECHAPROXIMA = "fechaProxima";
        public static final String FABRICANTE = "fabricante";
        public static final String LOTE = "lote";
        public static final String ESPECIALISTA = "especialista";
    }

    @Override
    public String NODE_NAME() {
        return NODE_NAME;
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(Key.ID, getId());
        map.put(Key.BIOLOGICO, getBiologico());
        map.put(Key.DOSIS, getDosis());
        map.put(Key.FECHA, getFecha());
        map.put(Key.FECHAPROXIMA, getFechaProxima());
        map.put(Key.FABRICANTE, getFabricante());
        map.put(Key.LOTE, getLote());
        map.put(Key.ESPECIALISTA, getEspecialista());
        return map;
    }

}
