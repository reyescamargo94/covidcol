package com.example.covidcol.persistencia.model;

import java.util.HashMap;
import java.util.Map;

public class PuntoCovidModel extends Model {

    public static String NODE_NAME = "punto_Covid";

    private String idTipoCovid;
    private String idUbicacion;

    public static class Key {
        public static final String ID = Model.Key.ID;
        public static final String ID_TIPO_COVID = "idTipoCOVID";
        public static final String ID_UBICACION = "idUbicacion";
    }

    public PuntoCovidModel(){}

    @Override
    public String NODE_NAME() {
        return PuntoCovidModel.NODE_NAME;
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(Key.ID, getId());
        map.put(Key.ID_TIPO_COVID, getIdTipoCovid());
        map.put(Key.ID_UBICACION, getIdUbicacion());
        return map;
    }

    public PuntoCovidModel(String id, String idTipoCovid, String idUbicacion) {
        super(id);
        this.idTipoCovid = idTipoCovid;
        this.idUbicacion = idUbicacion;
    }

    public String getIdTipoCovid() {
        return idTipoCovid;
    }

    public void setIdTipoCOVID(String idTipoCovid) {
        this.idTipoCovid = idTipoCovid;
    }

    public String getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(String idUbicacion) {
        this.idUbicacion = idUbicacion;
    }
}
