package com.example.covidcol.util;


import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.preference.PreferenceManager;
import android.widget.EditText;

import com.example.covidcol.R;
import com.example.covidcol.persistencia.model.PuntoInteresModel;
import com.example.covidcol.persistencia.model.PuntoInteresModel.Key;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated, DateFormat.getDateTimeInstance().format(new Date()));
    }

    public static PuntoInteresModel transforPuntoInteresModel(ObjetoParametros objetoParametros){
        PuntoInteresModel puntointeresModel = new PuntoInteresModel();
        puntointeresModel.setNombrePuntoInteres( objetoParametros.get(Key.NOMBRE_PUNTO_INTERES,String.class));
        puntointeresModel.setDireccion(objetoParametros.get(Key.DIRECCION,String.class));
        puntointeresModel.setCiudad(objetoParametros.get(Key.CIUDAD,String.class));
        puntointeresModel.setDepartamento(objetoParametros.get(Key.DEPARTAMENTO,String.class));
        puntointeresModel.setPais(objetoParametros.get(Key.PAIS,String.class));
        puntointeresModel.setCategoria(objetoParametros.get(Key.categoria,String.class));
        puntointeresModel.setTipo(objetoParametros.get(Key.tipo,String.class));
        puntointeresModel.setNombre(objetoParametros.get(Key.nombre,String.class));
        puntointeresModel.setUbicacion(objetoParametros.get(Key.UBICACION,String.class));
        puntointeresModel.setHorario(objetoParametros.get(Key.HORARIO,String.class));
        puntointeresModel.setNombrePrograma(objetoParametros.get(Key.NOMBRE_PROGRAMA,String.class));
        puntointeresModel.setPersonaContacto(objetoParametros.get(Key.PERSONA_CONTACTO,String.class));
        puntointeresModel.setEmail(objetoParametros.get(Key.EMAIL,String.class));
        Utils.setLatLng(puntointeresModel, puntointeresModel.getUbicacion());
        return puntointeresModel;
    }

    public static PuntoInteresModel setLatLng(PuntoInteresModel puntoInteresModel, String latLng){
        String lat = "", lng = "";
        lat = latLng.split(",")[0].replace("(", "").trim();
        lng = latLng.split(",")[1].replace(")", "").trim();
        // System.out.println(String.format("lat: %s lng: %s", lat,lng));
        puntoInteresModel.setLatitud(lat);
        puntoInteresModel.setLongitud(lng);
        // System.out.println(puntoInteresModel.getUbicacion());

        return puntoInteresModel;
    }

    public static List<PuntoInteresModel> transformPuntoInteresModels(Collection<ObjetoParametros> collection){
        return collection.stream()
                .map(objetoParametros -> transforPuntoInteresModel(objetoParametros))
                .collect(Collectors.toList());
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }
}