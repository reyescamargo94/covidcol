package com.example.covidcol.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.covidcol.R;
import com.example.covidcol.servicios.LocationService;

public class LoginActivity extends AppCompatActivity {

    EditText username, password;
    String user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
    }

    public void onClickPrincipal(View view){
        // Obtener valores de los controles
        user = username.getText().toString();
        pass = password.getText().toString();
        // Validar si el valor es nulo
        if(user.equals("")){
            username.setError("No puede estar en blanco");
        } // Validar si el valor es nulo
        else if(pass.equals("")){
            password.setError("No puede estar en blanco");
        }else{
            Intent intent = new Intent(getApplicationContext(), OpcionActivity.class);
            startActivity(intent);
        }


    }
}
